module.exports = {
	components: {
	},

	data() {
		return {
			dataFilter: '',
		};
	},

	methods: {
		changeFilterData(data){
			this.dataFilter = data;
			console.log("data changed to " + data);
		}
	},

	computed: {
		filter: function(){
			return this.dataFilter - 1;
		}
	},

	mounted(){
		console.log('checking');
	}
};