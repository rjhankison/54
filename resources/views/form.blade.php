@extends('layouts.app')

@section('content')

<div class="container">

	<form role="form" method="POST" action="{{url('post')}}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="form-group">

			<div class="col-md-6">
				<input type="text" class="input" name="text">

			</div>
		</div>
		<div class="form-group">

			<div class="col-md-6">
				<input type="file" class="input" name="file">

			</div>
		</div>
		<div class="form-group">
			<div class="col-md-6">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</div>
	</form>

</div>

@endsection