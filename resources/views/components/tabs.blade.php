<tabs inline-template>
	<div>
		<button type="button" class="btn btn-primary" @click="changeFilterData(1)">Data Set 1</button>
		<button type="button" class="btn btn-primary" @click="changeFilterData(2)">Data Set 2</button>
		<button type="button" class="btn btn-primary" @click="changeFilterData(3)">Data Set 3</button>
		@{{filter}}
	</div>
</tabs>