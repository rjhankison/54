@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				Testing
				
				@include('components.tabs')
				<chart chartData="5"></chart>
			</div>
		</div>
	</div>
</div>
@endsection
